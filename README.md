# Svelte mazes

A small experiment with maze generation and solving with [Svelte](https://svelte.dev/)

## How it works

Current version renders random maze and allows for it to be filled by clicking on any cell.
Clicked cell and all connected neighbors are filled.


![Video of filling](./assets/filling.mp4)


## Known bug

Bottom-most cells whjen clicked do not propagate up or right

