export enum CellSide {
	NONE = 0,
	RIGHT = 1 << 0,
	BOTTOM = 1 << 1,
	LEFT = 1 << 2,
	TOP = 1 << 3
}

export enum CellShape {
	EMPTY = 0,
	RIGHT = CellSide.RIGHT,
	BOTTOM = CellSide.BOTTOM,
	BOTTOM_AND_RIGHT = CellSide.BOTTOM | CellSide.RIGHT
}

export function packShapesToUint8Array(shapes: CellShape[]): Uint8Array {
	const newLen = Math.ceil(shapes.length / 4);
	const newArr = new Uint8Array(newLen);
	let i = 0;
	while (i < shapes.length) {
		const portion = shapes.slice(i, i + 4);
		portion.push(...new Array(4 - portion.length).fill(0));
		const val = portion.reduce((acc, curr) => (acc << 2) + curr, 0);
		newArr[i / 4] = val;
		i += 4;
	}
	return newArr;
}

export function packShapesToUint32Array(shapes: CellShape[]): Uint32Array {
	const newLen = Math.ceil(shapes.length / 16);
	const newArr = new Uint32Array(newLen);
	let i = 0;
	while (i < shapes.length) {
		const portion = shapes.slice(i, i + 16);
		portion.push(...new Array(16 - portion.length).fill(0));
		const val = portion.reduce((acc, curr) => (acc << 2) + curr, 0);
		newArr[i / 16] = val;
		i += 16;
	}
	return newArr;
}
